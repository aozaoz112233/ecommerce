package com.e_commerce.bob97.e_commerce.ui

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.e_commerce.bob97.e_commerce.R

class OrderItemViewHolder(view:View):RecyclerView.ViewHolder(view) {
    companion object {
        fun creat(parent: ViewGroup):OrderItemViewHolder{
            val  view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.order_item,parent,false)
            return OrderItemViewHolder(view)

        }
    }
}