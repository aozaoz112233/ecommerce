package com.e_commerce.bob97.e_commerce.repository.remote
import com.e_commerce.bob97.e_commerce.repository.data.Account
import com.e_commerce.bob97.e_commerce.repository.data.CateOrder
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Query

interface RemotePostCateOrderService {
    @POST(RemoteContract.POSTCATEORDER)
    fun postCateOrder(@Body cateOrder: CateOrder):Observable<HashMap<String,String>>
}
