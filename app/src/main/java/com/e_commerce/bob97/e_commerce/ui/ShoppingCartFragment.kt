package com.e_commerce.bob97.e_commerce.ui

import android.app.AlertDialog
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.e_commerce.bob97.e_commerce.GlideApp
import com.e_commerce.bob97.e_commerce.R
import com.e_commerce.bob97.e_commerce.viewmodel.ShoppingCartViewModel
import kotlinx.android.synthetic.main.shopping_cart_fragment.*


class ShoppingCartFragment : Fragment() {

    companion object {
        fun newInstance() = ShoppingCartFragment()
    }

    private lateinit var viewModel: ShoppingCartViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.shopping_cart_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ShoppingCartViewModel::class.java)
        initUi()
    }
    fun initUi()
    {
        val refresh = viewModel.allPrice().subscribe {
            allPrice.text = it
        }
        val glide = GlideApp.with(this)
        val adapter = ShoppingCartAdapter(glide,selectAllCheckbox,viewModel,allPrice)
        shoppingCartContent.adapter = adapter
        shoppingCartContent.layoutManager = LinearLayoutManager(context)
        shoppingCartEditor.setOnClickListener {
            when(shoppingCartEditor.text){
                "编辑" ->{
                    balance.text = "删除"
                    shoppingCartEditor.text = "完成"
                }
                "完成" ->{
                    balance.text = "订单提交"
                    shoppingCartEditor.text = "编辑"
                }
            }

        }


        balance.setOnClickListener {
            when(balance.text){
                "删除" ->{
                    viewModel.deleteCart()
                    adapter.notifyDataSetChanged()
                }
                "订单提交" ->{
                    if (viewModel.contex.isLogin()) {
                        val dispose = viewModel.postCateOrder().subscribe {
                            if (it.containsValue("success")){
                                val alert = AlertDialog.Builder(this.context)
                                        .setMessage("生成订单成功！")
                                        .show()
                                viewModel.deleteCart()
                                adapter.notifyDataSetChanged()
                                allPrice.text = "合计:0 (包含运费0)"
                            } else {
                                val alert = AlertDialog.Builder(this.context)
                                        .setMessage("订单生成失败，请求殴打后台")
                                        .show()
                            }
                        }
                    }
                    else{
                        val alert = AlertDialog.Builder(this.context)
                                .setMessage("请先登录")
                                .show()
                    }
                }
            }

        }

    }
}
