package com.e_commerce.bob97.e_commerce.ui

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment
import com.e_commerce.bob97.e_commerce.R
import com.e_commerce.bob97.e_commerce.repository.data.Cate
import com.e_commerce.bob97.e_commerce.viewmodel.ClassificationViewModel
import io.reactivex.Observable
import kotlinx.android.synthetic.main.classification_fragment.*


class ClassificationFragment : Fragment() {

    companion object {
        fun newInstance() = ClassificationFragment()
    }

    private lateinit var viewModel: ClassificationViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.classification_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ClassificationViewModel::class.java)
        initUi()

    }
    fun initUi() {
        val dispose = viewModel.getClassificationInfo().subscribe{
            val adapter = ClassificaitonViewPagerAdapter(childFragmentManager,it)
            classificationViewPager.adapter = adapter
            classificationTab.setupWithViewPager(classificationViewPager)
        }
    }


}
