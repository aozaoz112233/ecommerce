package com.e_commerce.bob97.e_commerce.repository.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.e_commerce.bob97.e_commerce.repository.data.Account
import com.e_commerce.bob97.e_commerce.repository.local.dao.AccountDao

@Database(entities = arrayOf(Account::class),version = 1)
abstract class AppDatabase:RoomDatabase() {
    companion object {
        fun onCreat(context: Context,useInMemory : Boolean):AppDatabase{
            val databaseBuilder = if (useInMemory){
                Room.inMemoryDatabaseBuilder(context,AppDatabase::class.java)
            }
            else {
                Room.databaseBuilder(context,AppDatabase::class.java,"AppDatabaseV2")
            }
            return databaseBuilder.build()
        }

    }
    abstract fun account():AccountDao

}