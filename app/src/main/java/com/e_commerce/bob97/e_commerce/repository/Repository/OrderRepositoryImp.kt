package com.e_commerce.bob97.e_commerce.repository.Repository

import android.util.Log
import com.e_commerce.bob97.e_commerce.repository.data.CateOrder
import com.e_commerce.bob97.e_commerce.repository.data.Order
import com.e_commerce.bob97.e_commerce.repository.remote.RemoteGetOrderImp
import com.e_commerce.bob97.e_commerce.repository.remote.RemotePostCateOrderImp
import com.e_commerce.bob97.e_commerce.repository.remote.RemotePostOrderOperationImp
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class OrderRepositoryImp @Inject constructor(private val remotePostCateOrderImp: RemotePostCateOrderImp,
private val remotePostOrderOperationImp: RemotePostOrderOperationImp,
private val remoteGetOrderImp: RemoteGetOrderImp) :OrderRepository  {
    override fun postOrderOperation(orderId: String, orderState: String): Observable<HashMap<String, String>> =
            remotePostOrderOperationImp.postOrderOperation(orderId,orderState)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())


    override fun getOrder(userId: String): Observable<List<Order>> =
        remoteGetOrderImp.getOrder(userId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())


    override fun postCateOrder(cateOrder: CateOrder) =
            remotePostCateOrderImp.postCateOrder(cateOrder)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

}