package com.e_commerce.bob97.e_commerce.repository.Repository

import android.app.Dialog
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.e_commerce.bob97.e_commerce.repository.data.Account
import com.e_commerce.bob97.e_commerce.repository.local.AppDatabase
import com.e_commerce.bob97.e_commerce.repository.remote.RemoteGetCodeImp
import com.e_commerce.bob97.e_commerce.repository.remote.RemoteGetUserImp
import com.e_commerce.bob97.e_commerce.repository.remote.RemotePostRegisterImp
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class UserRepositoryImp @Inject constructor(private val remoteGetUserImp: RemoteGetUserImp,
private val remoteGetCodeImp: RemoteGetCodeImp,
private val remotePostRegisterImp: RemotePostRegisterImp): UserRepository {
    override fun getCode(): Observable<HashMap<String, String>> {
        return remoteGetCodeImp.getCode()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }


    override fun getUserInfo(phone:String,password:String): LiveData<Account> {
        val userInfo = MutableLiveData<Account>()
        val disposable = remoteGetUserImp.getUserInfo(phone,password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{
                    userInfo.value = it }
        return userInfo
    }

    override fun postResiter(userTelphone: String, userPassword: String): Observable<HashMap<String, String>> =
        remotePostRegisterImp.postPostRegister(userTelphone,userPassword)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())


    override fun dbInsertUserInfo(db:AppDatabase,account: Account?) {
        Completable.fromAction{db.account().insert(account) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
    }


    override fun dbGetUserInfo(db:AppDatabase): Account {
        return db.account().getAccountInfo()
    }



}