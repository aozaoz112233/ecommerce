package com.e_commerce.bob97.e_commerce.di

import com.e_commerce.bob97.e_commerce.ui.ShoppingCartAdapter
import com.e_commerce.bob97.e_commerce.viewmodel.*
import dagger.Component
import javax.inject.Singleton

@Component(modules = arrayOf(AppModule::class,RemoteModule::class))
@Singleton interface AppComponent {
    fun inject(classificationPageViewModel: ClassificationPageViewModel)
    fun inject(personalViewModel: PersonalViewModel)
    fun inject(loginViewModel: LoginViewModel)
    fun inject(productPageViewModel: ProductPageViewModel)
    fun inject(commodityViewModel: CommodityViewModel)
    fun inject(shoppingCartViewModel: ShoppingCartViewModel)
    fun inject(classificationViewModel: ClassificationViewModel)
    fun inject(shoppingCartAdapter: ShoppingCartAdapter)
    fun inject(productListViewModel: ProductListViewModel)
    fun inject(orderPageViewModel: OrderPageViewModel)
    fun inject(registerViewModel: RegisterViewModel)


}