package com.e_commerce.bob97.e_commerce.ui

import android.app.Activity
import android.app.AlertDialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment
import com.e_commerce.bob97.e_commerce.GlideApp
import com.e_commerce.bob97.e_commerce.R
import com.e_commerce.bob97.e_commerce.repository.local.Entity.ShoppingCartItem
import com.e_commerce.bob97.e_commerce.repository.remote.RemoteContract
import com.e_commerce.bob97.e_commerce.viewmodel.CommodityViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.commodity_fragment.*


class CommodityFragment : Fragment() {

    companion object {
        fun newInstance() = CommodityFragment()
    }

    private lateinit var viewModel: CommodityViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.commodity_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(CommodityViewModel::class.java)
        initUi()
    }
    fun initUi(){
        (activity as Activity).bottomNavigationView.visibility = View.GONE
        commodityToolbar.inflateMenu(R.menu.commodity_bar)
        val  proId = arguments?.getInt("proId")?:1
        viewModel.getCommodityInfo(proId)!!.observe(this, Observer {
            commodityTitle.text = it!!.proName
            commodityDescribe.text = it.proDesp
            commodityPrice.text = "¥"+it.proPrice.toString()
            GlideApp.with(this)
                    .load(RemoteContract.IMAGEHOST+it.proImg)
                    .into(productImage)
            proPress.text = (it.proPress + ":"+ it.proPressCost)
            proMonthlySales.text = it.proMonthlySales.toString()
            proPlace.text = it.proPlace
            val dispose = it
            settlement.setOnClickListener {
         viewModel.innsertCart(proId,dispose.proName,dispose.proPrice,dispose.proPressCost,dispose.proImg,1)
                val alert = AlertDialog.Builder(this.context)
                        .setMessage("添加购物车成功")
                        .show()
            }
            commodityToolbar.setNavigationOnClickListener {
                (activity as Activity).bottomNavigationView.visibility = View.VISIBLE
                NavHostFragment.findNavController(this).navigateUp()
            }
        })
    }

}
