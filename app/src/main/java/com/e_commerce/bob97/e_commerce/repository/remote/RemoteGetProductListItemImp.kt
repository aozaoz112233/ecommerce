package com.e_commerce.bob97.e_commerce.repository.remote

import com.e_commerce.bob97.e_commerce.repository.data.Product
import io.reactivex.Observable
import javax.inject.Inject

class RemoteGetProductListItemImp @Inject constructor(private val remoteGetProductListItemService: RemoteGetProductListItemService){
     fun getProductListItem(cateId:Int) =
             remoteGetProductListItemService.getProductListItem(cateId)
}