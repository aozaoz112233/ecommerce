package com.e_commerce.bob97.e_commerce.repository.Repository

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Database
import com.e_commerce.bob97.e_commerce.repository.data.Account
import com.e_commerce.bob97.e_commerce.repository.local.AppDatabase
import io.reactivex.Observable

interface UserRepository {
    fun getUserInfo(phone:String,password:String):LiveData<Account>
    fun dbInsertUserInfo(db:AppDatabase,account: Account?)
    fun dbGetUserInfo(db:AppDatabase):Account
    fun postResiter(userTelphone:String,userPassword:String): Observable<HashMap<String, String>>
    fun getCode():Observable<HashMap<String,String>>
}