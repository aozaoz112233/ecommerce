package com.e_commerce.bob97.e_commerce.ui

import android.arch.paging.PagedListAdapter
import android.os.Bundle
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.e_commerce.bob97.e_commerce.GlideRequests
import com.e_commerce.bob97.e_commerce.repository.data.Product

class ProductListItemAdapter(private val glide: GlideRequests,private val didSelect:(proIdBundle: Bundle)->Unit):PagedListAdapter<Product,RecyclerView.ViewHolder>(diffUtil){
    companion object {
        private val diffUtil = object :DiffUtil.ItemCallback<Product>(){
            override fun areContentsTheSame(oldItem: Product?, newItem: Product?): Boolean =
                    oldItem == newItem

            override fun areItemsTheSame(oldItem: Product?, newItem: Product?): Boolean =
                    oldItem?.proId== newItem?.proId
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ProductListItemViewHolder.creat(parent,glide,didSelect)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ProductListItemViewHolder).bind(getItem(position))
    }


}