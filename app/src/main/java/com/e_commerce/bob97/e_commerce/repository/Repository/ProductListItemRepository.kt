package com.e_commerce.bob97.e_commerce.repository.Repository

import com.e_comme.rce.bob97.e_commerce.repository.data.Listing
import com.e_commerce.bob97.e_commerce.repository.data.Cate
import com.e_commerce.bob97.e_commerce.repository.data.Product
import io.reactivex.Observable

interface ProductListItemRepository {
    fun getProductListItem(cateId:Int): Listing<Product>
    fun getProductList(pId:Int): Observable<MutableList<Cate>>
}