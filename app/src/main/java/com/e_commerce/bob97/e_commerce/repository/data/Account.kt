package com.e_commerce.bob97.e_commerce.repository.data

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName


@Entity(tableName = "account")
data class Account(
        @PrimaryKey
        @SerializedName("userId")val userId: Int,
        @SerializedName("userName")val userName: String,
        @SerializedName("userPassword")val userPassword: String,
        @SerializedName("userImg")val userImg: String,
        @SerializedName("userTelphone")val userTelphone: String,
        @SerializedName("userSex")val userSex: String,
        @SerializedName("userMail")val userMail: String,
        @SerializedName("userBirthday")val userBirthday: String,
        @SerializedName("userRegistrationTime")val userRegistrationTime: String
)