package com.e_commerce.bob97.e_commerce.repository.remote

import javax.inject.Inject

class RemoteGetClassificationItemImp @Inject constructor(private val remoteGetClassificationItemService: RemoteGetClassificationItemService) {
    fun getGetClassificationItem(pId:String) =
        remoteGetClassificationItemService.getClassificationItem(pId)

}