package com.e_commerce.bob97.e_commerce.di

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.e_commerce.bob97.e_commerce.repository.data.Account
import com.e_commerce.bob97.e_commerce.repository.local.Entity.ShoppingCartItem
import io.reactivex.Observable

class EconApplication:Application() {
    companion object {
        lateinit var appComponent: AppComponent
        lateinit var loginInfo:Account
        var shoppingCatItems:MutableList<ShoppingCartItem> = mutableListOf()

        var isLogin:Boolean = false
    }

    override fun onCreate() {
        super.onCreate()
        initializeDagger()
    }
    fun initializeDagger(){
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .remoteModule(RemoteModule())
                .build()

    }
    fun insertAccount(account: Account){
        loginInfo = account
        isLogin = true
    }
    fun insertShoppingCat(shoppingCartItem: ShoppingCartItem){
        shoppingCatItems.add(shoppingCartItem)
    }
    fun getShoppingCatItems() = shoppingCatItems
    fun getAccount() = loginInfo
    fun isLogin() = isLogin


}