package com.e_commerce.bob97.e_commerce.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel;
import com.e_commerce.bob97.e_commerce.di.EconApplication
import com.e_commerce.bob97.e_commerce.repository.Repository.ClassificationItemRepositoryImp
import javax.inject.Inject

class ClassificationPageViewModel : ViewModel() {
  @Inject lateinit var  repository: ClassificationItemRepositoryImp
    init {
        initializeDagger()
    }

    private  val subItemIdx = MutableLiveData<String>()
    fun getItem(idx:String) =
    repository.getClassificationItem(idx)?.pagedList

    private fun initializeDagger() = EconApplication.appComponent.inject(this)

}
