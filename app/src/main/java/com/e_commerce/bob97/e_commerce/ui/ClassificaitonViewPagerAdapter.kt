package com.e_commerce.bob97.e_commerce.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.util.Log
import com.e_commerce.bob97.e_commerce.repository.data.Cate
import io.reactivex.Observable

class ClassificaitonViewPagerAdapter(fm:FragmentManager?,val list:MutableList<Cate>):FragmentPagerAdapter(fm){

    override fun getItem(position: Int): Fragment {
        val fragment = ClassificationPageFragment.newInstance()
        val bundle = Bundle()
        bundle.putString("idx",list[position].cateId.toString())
        bundle.putString("image",list[position].cateIcon)
        bundle.putString("title","━━━━━  "+list[position].cateName+"分类"+"  ━━━━━")
        fragment.arguments = bundle
        return fragment
    }

    override fun getCount(): Int {

        return list.size
    }

    override fun getPageTitle(position: Int): CharSequence? {

        return list[position].cateName

    }

}