package com.e_commerce.bob97.e_commerce.repository.data

import com.e_commerce.bob97.e_commerce.repository.local.Entity.ShoppingCartItem
import com.google.gson.annotations.SerializedName

data class CateOrder(
        @SerializedName("userId")  var userId:Int,
        @SerializedName("orderItems") var orderItems:List<ShoppingCartItem>
)