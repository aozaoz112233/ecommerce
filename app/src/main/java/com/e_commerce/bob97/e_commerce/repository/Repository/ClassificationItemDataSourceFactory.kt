package com.e_commerce.bob97.e_commerce.repository.Repository

import android.arch.paging.DataSource
import com.e_commerce.bob97.e_commerce.repository.data.Cate
import com.e_commerce.bob97.e_commerce.repository.remote.RemoteGetClassificationItemImp
import javax.inject.Inject


class ClassificationItemDataSourceFactory@Inject constructor(private val remoteGetClassificationItemImp: RemoteGetClassificationItemImp, private val index:String):DataSource.Factory<String,Cate>() {
    override fun create(): DataSource<String, Cate> {
        val source = ClassificationItemDataSource(remoteGetClassificationItemImp,index)
        return source
    }

}