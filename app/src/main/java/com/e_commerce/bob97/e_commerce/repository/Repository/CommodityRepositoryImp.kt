package com.e_commerce.bob97.e_commerce.repository.Repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.e_commerce.bob97.e_commerce.repository.data.Product
import com.e_commerce.bob97.e_commerce.repository.remote.RemoteGetCommodityImp
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CommodityRepositoryImp @Inject constructor(private val remoteGetCommodityImp: RemoteGetCommodityImp)  :CommodityRepository {
    override fun getCommodityInfo(proId: Int): LiveData<Product> {
        val commodityInfo = MutableLiveData<Product>()
        val dispose = remoteGetCommodityImp.getCommodity(proId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ({
                    commodityInfo.value = it
                },{
                    Log.d("getCommodity",it.localizedMessage)
                })
        return commodityInfo
    }
}