package com.e_commerce.bob97.e_commerce.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel;
import com.e_commerce.bob97.e_commerce.di.EconApplication
import com.e_commerce.bob97.e_commerce.repository.Repository.CommodityRepositoryImp
import com.e_commerce.bob97.e_commerce.repository.data.Product
import com.e_commerce.bob97.e_commerce.repository.local.Entity.ShoppingCartItem
import com.e_commerce.bob97.e_commerce.repository.remote.RemoteGetCommodityImp
import com.e_commerce.bob97.e_commerce.repository.remote.RemoteGetCommodityService
import javax.inject.Inject

class CommodityViewModel : ViewModel() {
    @Inject
    lateinit var contex:EconApplication
    @Inject
    lateinit var commodityRepositoryImp:CommodityRepositoryImp

    private var productInfo:LiveData<Product>? = null
    init {
        initializeDagger()
    }

    fun getCommodityInfo(proId: Int):LiveData<Product>?{
        if (productInfo == null)
            productInfo = MutableLiveData()
        productInfo = commodityRepositoryImp.getCommodityInfo(proId)
        return productInfo
    }

    fun innsertCart(proId:Int,proName: String,proPrice: Double, proPressCost: Double,proImg: String,proCount:Int){
        contex.insertShoppingCat(ShoppingCartItem(proId,proName,proPrice,proPressCost,proImg,proCount))
    }


    private fun initializeDagger() = EconApplication.appComponent.inject(this)
}
