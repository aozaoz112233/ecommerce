package com.e_commerce.bob97.e_commerce.repository.data

import com.google.gson.annotations.SerializedName

data class Cate(
        @SerializedName("cateId")val cateId: Int,
        @SerializedName("cateName")val cateName: String,
        @SerializedName("catePname")val catePname: String,
        @SerializedName("catePid")val catePid: Int,
        @SerializedName("cateIcon")val cateIcon: String
)