package com.e_commerce.bob97.e_commerce.ui

import android.app.AlertDialog
import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment
import com.e_commerce.bob97.e_commerce.GlideRequests
import com.e_commerce.bob97.e_commerce.R
import com.e_commerce.bob97.e_commerce.repository.data.Order
import com.e_commerce.bob97.e_commerce.viewmodel.OrderPageViewModel
import kotlinx.android.synthetic.main.order_item.view.*

class OrderItemAdaper(private val context:Context?,private val glide: GlideRequests,private val orders:List<Order>,private val viewmodel: OrderPageViewModel,private val fragment:OrderPageFragment):RecyclerView.Adapter<OrderItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderItemViewHolder {
        return OrderItemViewHolder.creat(parent)
    }

    override fun getItemCount(): Int {
        return orders.size

    }

    override fun onBindViewHolder(holder: OrderItemViewHolder, position: Int) {
        holder.itemView.orderIdText.text = orders[position].orderId
        holder.itemView.orderTimeText.text = orders[position].orderTime
        holder.itemView.productNum.text = "共"+orders[position].orderItems.size.toString()+"件商品"
        holder.itemView.orderPriceSum.text = "合计：￥"+orders[position].orderSumPrice.toString()
        holder.itemView.orderItemContent.adapter = OrderItemContentAdapter(glide,orders[position].orderItems)
        holder.itemView.orderItemContent.layoutManager = LinearLayoutManager(context)
        when(orders[position].orderState){
            "新订单" ->
                holder.itemView.orderOperation.setOnClickListener {
                    val dispose = viewmodel.postOrderOperation(orders[position].orderId,"已付款")
                            .subscribe {
                                if (it.containsValue("success")){
                                    val alert = AlertDialog.Builder(this.context)
                                            .setMessage("支付成功")
                                            .show()
                                    NavHostFragment.findNavController(fragment).navigate(R.id.action_orderFragment_self)
                                }
                                else {
                                    val alert = AlertDialog.Builder(this.context)
                                            .setMessage("支付失败")
                                            .show()
                                }
                            }
                }
            "已付款" ->
                holder.itemView.orderOperation.text = "发货详细信息"
        }
    }
}