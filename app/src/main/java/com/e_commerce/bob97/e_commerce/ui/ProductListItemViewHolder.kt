package com.e_commerce.bob97.e_commerce.ui

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.e_commerce.bob97.e_commerce.GlideRequests
import com.e_commerce.bob97.e_commerce.R
import com.e_commerce.bob97.e_commerce.repository.data.Product
import com.e_commerce.bob97.e_commerce.repository.remote.RemoteContract

class ProductListItemViewHolder(view: View, private val glide: GlideRequests,internal val didSelect:(proIdBundle:Bundle) -> Unit):RecyclerView.ViewHolder(view) {
    private val image:ImageView = view.findViewById(R.id.productListItemImage)
    private val title:TextView = view.findViewById(R.id.productListItemTitle)
    private val price:TextView = view.findViewById(R.id.productListItemPrice)
    private var track:Product? = null
    private var proId:Int? = null
    init {
        view.setOnClickListener{
            val bundle = Bundle()
            bundle.putInt("proId",proId!!)
            didSelect(bundle)

        }
    }

    fun bind(post: Product?){
        this.track = post
        title.text = post?.proName
        glide.load(RemoteContract.IMAGEHOST + post?.proImg)
                .centerCrop()
                .into(image)
        price.text = post?.proPrice.toString()
        proId = post?.proId
    }

    companion object {
        fun creat(parent:ViewGroup,glide: GlideRequests,didSelect:(proIdBundle:Bundle)->Unit):ProductListItemViewHolder{
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_product,parent,false)
            return ProductListItemViewHolder(view,glide,didSelect)

        }
    }

}