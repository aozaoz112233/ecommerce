package com.e_commerce.bob97.e_commerce.repository.Repository

import com.e_commerce.bob97.e_commerce.repository.data.CateOrder
import com.e_commerce.bob97.e_commerce.repository.data.Order
import io.reactivex.Observable

interface OrderRepository {
    fun postCateOrder(cateOrder: CateOrder):Observable<HashMap<String,String>>
    fun getOrder(userId:String):Observable<List<Order>>
    fun postOrderOperation(orderId:String,orderState:String):Observable<HashMap<String,String>>
}