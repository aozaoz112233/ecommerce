package com.e_commerce.bob97.e_commerce.ui

import android.app.Fragment
import android.content.Context
import android.support.v7.widget.AppCompatCheckBox
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.TextView
import com.e_commerce.bob97.e_commerce.GlideRequests
import com.e_commerce.bob97.e_commerce.di.EconApplication
import com.e_commerce.bob97.e_commerce.viewmodel.ShoppingCartViewModel
import kotlinx.android.synthetic.main.item_shoppingcart.view.*
import kotlinx.android.synthetic.main.shopping_cart_fragment.*
import kotlinx.android.synthetic.main.shopping_cart_fragment.view.*
import javax.inject.Inject

class ShoppingCartAdapter(private val glide: GlideRequests,val checkBox: AppCompatCheckBox,val viewModel: ShoppingCartViewModel,val allPrice:TextView) :RecyclerView.Adapter<ShoppingViewHolder>() {
    @Inject
    lateinit var contex: EconApplication
    init {
        initializeDagger()

    }



    override fun getItemCount(): Int {
        return contex.getShoppingCatItems().size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShoppingViewHolder {
        return ShoppingViewHolder.creat(parent, glide)
    }



    override fun onBindViewHolder(holder: ShoppingViewHolder, position: Int) {
        holder.bind(contex.getShoppingCatItems()[position])

        fun refresh() = viewModel.allPrice().subscribe {
            allPrice.text = it
        }
        checkBox.setOnClickListener {
            viewModel.selectAll(checkBox.isChecked)
            notifyDataSetChanged()
            refresh()
        }

        fun getNum():Int = holder.itemView.item_num.text.toString().toInt()

        holder.itemView.button_add.setOnClickListener {
            if (getNum() < 50){
                holder.itemView.item_num.setText((getNum() + 1).toString())
                contex.getShoppingCatItems()[position].proCount++
                refresh()
            }
        }
        holder.itemView.button_cut.setOnClickListener {
            if (getNum() > 1 ){
                holder.itemView.item_num.setText((getNum() - 1).toString())
                contex.getShoppingCatItems()[position].proCount--
                refresh()
            }
        }
        holder.itemView.cartItemCheckbox.setOnClickListener {
            contex.getShoppingCatItems()[position].onClick = !contex.getShoppingCatItems()[position].onClick
            var flag: Boolean = true
            contex.getShoppingCatItems().map { flag = flag && it.onClick }
            checkBox.isChecked = flag
            refresh()
        }


    }
    private fun initializeDagger() = EconApplication.appComponent.inject(this)

}