package com.e_commerce.bob97.e_commerce.repository.remote

import javax.inject.Inject

class RemoteGetClassificationImp @Inject constructor(private val remoteGetClassificationService: RemoteGetClassificationService) {
    fun getGetClassification() =
            remoteGetClassificationService.getClassification()

}