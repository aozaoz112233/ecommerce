package com.e_commerce.bob97.e_commerce.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel;
import android.util.Base64
import com.e_commerce.bob97.e_commerce.R
import com.e_commerce.bob97.e_commerce.di.EconApplication
import com.e_commerce.bob97.e_commerce.repository.Repository.UserRepositoryImp
import com.e_commerce.bob97.e_commerce.repository.data.Account
import com.e_commerce.bob97.e_commerce.repository.local.AppDatabase
import java.nio.charset.StandardCharsets
import java.security.KeyFactory
import java.security.spec.X509EncodedKeySpec
import javax.inject.Inject

class LoginViewModel : ViewModel() {
    @Inject
    lateinit var userRepositoryImp: UserRepositoryImp
    @Inject
    lateinit var contex:EconApplication

    var editTextCheck = mutableMapOf<String, Boolean>(Pair("phone", false),Pair("password", false))

    private var userinfo: LiveData<Account>? = null
    private val db by lazy{
        AppDatabase.onCreat(context = contex,useInMemory = false)
    }
    init {
        initializeDagger()
    }
        fun RemoteloadUserinfo(phone:String,password:String): LiveData<Account>?{
        if (userinfo == null)
            userinfo = MutableLiveData()
        val dispose = userRepositoryImp.getCode().subscribe {
            val publicKey = KeyFactory.getInstance("RSA").generatePublic(X509EncodedKeySpec(Base64.decode(it.get("publicKey"),Base64.DEFAULT)))
            userinfo = userRepositoryImp.getUserInfo(RSACrypt.encryptByPublicKey(phone,publicKey),RSACrypt.encryptByPublicKey(password,publicKey))
        }
        return userinfo
    }










        fun getCode() = userRepositoryImp.getCode()

    fun setEditTextLisnerlistener(listener:(isClickable:Boolean,color:Int)->Unit) {
        if (!editTextCheck.containsValue(false))
        listener(true,R.color.colorButton)
        else
            listener(false,R.color.colorWhite)
    }



    fun saveUserinfo(account: Account?){
        userRepositoryImp.dbInsertUserInfo(db = db,account = account)
        contex.insertAccount(account!!)
    }



    private fun initializeDagger() = EconApplication.appComponent.inject(this)
}
