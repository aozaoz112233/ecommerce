package com.e_commerce.bob97.e_commerce.viewmodel

import android.arch.lifecycle.ViewModel;
import com.e_commerce.bob97.e_commerce.di.EconApplication
import com.e_commerce.bob97.e_commerce.repository.Repository.ClassificationItemRepositoryImp
import com.e_commerce.bob97.e_commerce.repository.Repository.ProductListItemRepositoryImp
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ProductListViewModel : ViewModel() {
    @Inject
    lateinit var productListitemRepositoryImp: ProductListItemRepositoryImp

    init {
        initializeDagger()
    }

    fun getClassificationInfo(pId:Int) = productListitemRepositoryImp.getProductList(pId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())


    private fun initializeDagger() = EconApplication.appComponent.inject(this)
}
