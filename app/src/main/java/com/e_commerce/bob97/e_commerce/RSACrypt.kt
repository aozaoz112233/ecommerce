import android.util.Base64
import java.io.ByteArrayOutputStream
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.security.KeyPairGenerator
import java.security.PrivateKey
import java.security.PublicKey
import javax.crypto.Cipher


/**
 * 非对称加密RSA加密和解密
 */
object RSACrypt {
    private val transformation = "RSA/ECB/PKCS1Padding"
    private val ENCRYPT_MAX_SIZE = 117 //加密：每次最大加密长度117个字节
    private val DECRYPT_MAX_SIZE = 128 //解密：每次最大解密长度128个字节

    /**
     * 私钥加密
     */
    fun encryptByPrivateKey(input: String, privateKey: PrivateKey): String {
        /********************非对称加/解密三部曲**********************/
        //1.创建cipher对象
        val cipher = Cipher.getInstance(transformation)
        //2.初始化cipher
        cipher.init(Cipher.ENCRYPT_MODE, privateKey)
        //3.加密/解密
        val encrypt = cipher.doFinal(input.toByteArray())
        return Base64.encodeToString(encrypt,Base64.DEFAULT)
    }

    /**
     * 公钥解密
     */
    fun decryptByPublicKey(input: String, publicKey: PublicKey): String {
        val decode = Base64.decode(input,Base64.DEFAULT)
        /********************非对称加/解密三部曲**********************/
        //1.创建cipher对象
        val cipher = Cipher.getInstance(transformation)
        //2.初始化cipher
        cipher.init(Cipher.DECRYPT_MODE, publicKey)
        //3.加密/解密
        val encrypt = cipher.doFinal(decode)
        return String(encrypt)
    }

    /**
     * 公钥加密
     */
    fun encryptByPublicKey(input: String, publicKey: PublicKey): String {
        /********************非对称加/解密三部曲**********************/
        //1.创建cipher对象
        val cipher = Cipher.getInstance(transformation)
        //2.初始化cipher
        cipher.init(Cipher.ENCRYPT_MODE, publicKey)
        //3.加密/解密
        val encrypt = cipher.doFinal(input.toByteArray(StandardCharsets.UTF_8))
        return Base64.encodeToString(encrypt,Base64.NO_WRAP)
    }

    /**
     * 私钥解密
     */
    fun decryptByPrivateKey(input: String, privateKey: PrivateKey): String {
        val decode = Base64.decode(input,Base64.DEFAULT)
        /********************非对称加/解密三部曲**********************/
        //1.创建cipher对象
        val cipher = Cipher.getInstance(transformation)
        //2.初始化cipher
        cipher.init(Cipher.DECRYPT_MODE, privateKey)
        //3.加密/解密
        val encrypt = cipher.doFinal(decode)
        return String(encrypt)
    }

    /**
     * 私钥分段加密
     */
    fun encryptByPrivateKey2(input: String, privateKey: PrivateKey): String {
        val byteArray = input.toByteArray()

        var temp:ByteArray
        var offset = 0 //当前偏移的位置

        val bos = ByteArrayOutputStream()

        /********************非对称加/解密三部曲**********************/
        //1.创建cipher对象
        val cipher = Cipher.getInstance(transformation)
        //2.初始化cipher
        cipher.init(Cipher.ENCRYPT_MODE, privateKey)
        //3.加密：分段加密
//        val encrypt = cipher.doFinal()

        while (byteArray.size - offset >0) { //没有加密完
            //每次最大加密117个字节
            if(byteArray.size - offset >= ENCRYPT_MAX_SIZE){
                //剩余部分大于117,加密完整117
                temp  = cipher.doFinal(byteArray, offset, ENCRYPT_MAX_SIZE)
                offset+= ENCRYPT_MAX_SIZE
            }else{
                //加密最后一块
                temp  = cipher.doFinal(byteArray, offset, byteArray.size - offset)
                offset = byteArray.size
            }
            //存储到临时缓冲区
            bos.write(temp)
        }
        bos.close()

        return Base64.encodeToString(bos.toByteArray(),Base64.DEFAULT)
    }

    /**
     * 公钥分段解密
     */
    fun decryptByPublicKeyKey2(input: String, publicKey: PublicKey): String? {
        val byteArray = Base64.decode(input,Base64.DEFAULT)

        var temp:ByteArray
        var offset = 0 //当前偏移的位置

        val bos = ByteArrayOutputStream()

        /********************非对称加/解密三部曲**********************/
        //1.创建cipher对象
        val cipher = Cipher.getInstance(transformation)
        //2.初始化cipher
        cipher.init(Cipher.DECRYPT_MODE, publicKey)
        //3.加密：分段加密
//        val encrypt = cipher.doFinal()

        while (byteArray.size - offset >0) { //没有加密完
            //每次最大解密128个字节
            if(byteArray.size - offset >= DECRYPT_MAX_SIZE){
                //剩余部分大于128,解密完整128
                temp  = cipher.doFinal(byteArray, offset, DECRYPT_MAX_SIZE)
                offset+= DECRYPT_MAX_SIZE
            }else{
                //解密最后一块
                temp  = cipher.doFinal(byteArray, offset, byteArray.size - offset)
                offset = byteArray.size
            }
            //存储到临时缓冲区
            bos.write(temp)
        }
        bos.close()

        return String(bos.toByteArray())
    }

    /**
     * 公钥分段加密
     */
    fun encryptByPublicKey2(input: String, publicKey: PublicKey): String {
        val byteArray = input.toByteArray()

        var temp:ByteArray
        var offset = 0 //当前偏移的位置

        val bos = ByteArrayOutputStream()

        /********************非对称加/解密三部曲**********************/
        //1.创建cipher对象
        val cipher = Cipher.getInstance(transformation)
        //2.初始化cipher
        cipher.init(Cipher.ENCRYPT_MODE, publicKey)
        //3.加密：分段加密
//        val encrypt = cipher.doFinal()

        while (byteArray.size - offset >0) { //没有加密完
            //每次最大加密117个字节
            if(byteArray.size - offset >= ENCRYPT_MAX_SIZE){
                //剩余部分大于117,加密完整117
                temp  = cipher.doFinal(byteArray, offset, ENCRYPT_MAX_SIZE)
                offset+= ENCRYPT_MAX_SIZE
            }else{
                //加密最后一块
                temp  = cipher.doFinal(byteArray, offset, byteArray.size - offset)
                offset = byteArray.size
            }
            //存储到临时缓冲区
            bos.write(temp)
        }
        bos.close()

        return Base64.encodeToString(bos.toByteArray(),Base64.DEFAULT)
    }

    /**
     * 私钥分段解密
     */
    fun decryptByPrivateKey2(input: String, privateKey: PrivateKey): String? {
        val byteArray = Base64.decode(input,Base64.DEFAULT)

        var temp:ByteArray
        var offset = 0 //当前偏移的位置

        val bos = ByteArrayOutputStream()

        /********************非对称加/解密三部曲**********************/
        //1.创建cipher对象
        val cipher = Cipher.getInstance(transformation)
        //2.初始化cipher
        cipher.init(Cipher.DECRYPT_MODE, privateKey)
        //3.加密：分段加密
//        val encrypt = cipher.doFinal()

        while (byteArray.size - offset >0) { //没有解密完
            //每次最大解密128个字节
            if(byteArray.size - offset >= DECRYPT_MAX_SIZE){
                //剩余部分大于128,解密完整128
                temp  = cipher.doFinal(byteArray, offset, DECRYPT_MAX_SIZE)
                offset+= DECRYPT_MAX_SIZE
            }else{
                //解密最后一块
                temp  = cipher.doFinal(byteArray, offset, byteArray.size - offset)
                offset = byteArray.size
            }
            //存储到临时缓冲区
            bos.write(temp)
        }
        bos.close()

        return String(bos.toByteArray())
    }
}
