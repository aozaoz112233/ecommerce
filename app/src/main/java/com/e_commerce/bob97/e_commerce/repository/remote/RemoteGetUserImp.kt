package com.e_commerce.bob97.e_commerce.repository.remote

import javax.inject.Inject

class RemoteGetUserImp @Inject constructor(private val remoteGetUserService: RemoteGetUserService) {
    fun getUserInfo(phone:String,password:String) =
            remoteGetUserService.getLoginUser(phone,password)
}