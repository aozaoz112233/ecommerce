package com.e_commerce.bob97.e_commerce.repository.remote
import com.e_commerce.bob97.e_commerce.repository.data.Account
import io.reactivex.Observable
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface RemoteGetUserService {
    @FormUrlEncoded
    @POST(RemoteContract.LOGIN)
    fun getLoginUser(@Field("userTelphone") phone:String, @Field("userPassword") password:String):Observable<Account>
}
