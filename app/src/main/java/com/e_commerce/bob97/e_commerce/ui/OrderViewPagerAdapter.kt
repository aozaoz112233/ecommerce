package com.e_commerce.bob97.e_commerce.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class OrderViewPagerAdapter(fm:FragmentManager?):FragmentPagerAdapter(fm){

    val title = listOf<String>("全部","待付款","待发货","待收货","待评价")

    override fun getItem(position: Int): Fragment {
        val bundle = Bundle()
        bundle.putInt("postion",position)
      val fragment = OrderPageFragment.newInstance()
        fragment.arguments = bundle
       return fragment
    }

    override fun getCount(): Int {

        return title.size
    }

    override fun getPageTitle(position: Int): CharSequence? {

       return title[position]

    }

}