package com.e_commerce.bob97.e_commerce.repository.local.Entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "shopping_cart")
data class ShoppingCart (
        @SerializedName("userId")  var userId:Int,
        @SerializedName("shoppingCartId") var shoppingCartId:Int
)