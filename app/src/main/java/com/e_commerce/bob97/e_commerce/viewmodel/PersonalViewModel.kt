package com.e_commerce.bob97.e_commerce.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel;
import com.e_commerce.bob97.e_commerce.di.EconApplication
import com.e_commerce.bob97.e_commerce.repository.Repository.UserRepositoryImp
import com.e_commerce.bob97.e_commerce.repository.data.Account
import javax.inject.Inject

class PersonalViewModel() : ViewModel() {
    @Inject lateinit var userRepositoryImp: UserRepositoryImp
    @Inject lateinit var context:EconApplication

    private var userinfo:LiveData<Account>? = null

    init {
        initializeDagger()
        islogined()
    }

    fun islogined() = context.isLogin()

    fun logined() = context.getAccount()


    override fun onCleared() {
        super.onCleared()
    }
    private fun initializeDagger() = EconApplication.appComponent.inject(this)
}
