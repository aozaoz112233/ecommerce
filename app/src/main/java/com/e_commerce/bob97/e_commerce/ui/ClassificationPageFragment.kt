package com.e_commerce.bob97.e_commerce.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment
import com.e_commerce.bob97.e_commerce.GlideApp
import com.e_commerce.bob97.e_commerce.viewmodel.ClassificationPageViewModel
import com.e_commerce.bob97.e_commerce.R
import com.e_commerce.bob97.e_commerce.repository.remote.RemoteContract
import kotlinx.android.synthetic.main.classification_page_fragment.*
import kotlinx.android.synthetic.main.commodity_fragment.*


class ClassificationPageFragment() : Fragment() {

    companion object {
        fun newInstance() = ClassificationPageFragment()
    }

    private lateinit var viewModel: ClassificationPageViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.classification_page_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ClassificationPageViewModel::class.java)
        initUi()
    }
    fun initUi(){
        val Pid = arguments?.getString("idx")?:"1"
        val image = arguments?.getString("image")
        val title = arguments?.getString("title")
        val glide = GlideApp.with(this)
        val adapter = ClassificationItemAdapter(glide = glide,didSelect = {
            it ->
            NavHostFragment.findNavController(this.parentFragment!!).navigate(R.id.action_classificationFragment_to_productListFragment,it)
        })

        val layoutManager = GridLayoutManager(context,3)
        classificationPageRecyclerView.adapter = adapter
        classificationPageRecyclerView.layoutManager = layoutManager
        glide.load(RemoteContract.IMAGEHOST+image)
                .into(classificationPageImage)
        classificationPageText.text = title
        viewModel.getItem(Pid).observe(this, Observer {
            adapter.submitList(it)
        })

    }

}
