package com.e_commerce.bob97.e_commerce.repository.data
import com.e_commerce.bob97.e_commerce.repository.data.OrderItem
import com.google.gson.annotations.SerializedName

data class Order(
        @SerializedName("orderId")val orderId: String,
        @SerializedName("userId")val userId: Int,
        @SerializedName("orderSumPrice") val orderSumPrice: Int,
        @SerializedName("orderState")val orderState: String,
        @SerializedName("orderPostMenthod")val orderPostMenthod: String,
        @SerializedName("orderPostName")val orderPostName: String,
        @SerializedName("orderPostTelphone")val orderPostTelphone: String,
        @SerializedName("orderPostAddress")val orderPostAddress: String,
        @SerializedName("orderTime")val orderTime: String,
        @SerializedName("orderItems")val orderItems: List<OrderItem>
)


