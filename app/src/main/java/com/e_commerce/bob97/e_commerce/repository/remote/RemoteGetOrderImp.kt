package com.e_commerce.bob97.e_commerce.repository.remote

import javax.inject.Inject

class RemoteGetOrderImp @Inject constructor(private val remoteGetOrderSrvice: RemoteGetOrderSrvice){
    fun getOrder(userId:String) = remoteGetOrderSrvice.remoteGetOrderSrvice(userId)
}