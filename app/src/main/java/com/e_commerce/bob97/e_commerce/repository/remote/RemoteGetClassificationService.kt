package com.e_commerce.bob97.e_commerce.repository.remote

import com.e_commerce.bob97.e_commerce.repository.data.Cate
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface RemoteGetClassificationService {
    @POST(RemoteContract.CLASSIFICATION)
    fun getClassification():Observable<MutableList<Cate>>
}