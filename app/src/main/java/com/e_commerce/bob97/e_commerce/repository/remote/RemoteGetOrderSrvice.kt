package com.e_commerce.bob97.e_commerce.repository.remote

import com.e_commerce.bob97.e_commerce.repository.data.Order
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface RemoteGetOrderSrvice {
    @POST(RemoteContract.ORDER)
    fun remoteGetOrderSrvice(@Query("userId")userid:String):Observable<List<Order>>
}