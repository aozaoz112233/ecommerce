package com.e_commerce.bob97.e_commerce.repository.Repository

import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.e_comme.rce.bob97.e_commerce.repository.data.Listing
import com.e_commerce.bob97.e_commerce.repository.data.Cate
import com.e_commerce.bob97.e_commerce.repository.data.Product
import com.e_commerce.bob97.e_commerce.repository.remote.RemoteGetClassificationImp
import com.e_commerce.bob97.e_commerce.repository.remote.RemoteGetClassificationItemImp
import com.e_commerce.bob97.e_commerce.repository.remote.RemoteGetProductListItemImp
import io.reactivex.Observable
import javax.inject.Inject

class ProductListItemRepositoryImp @Inject constructor(private val remoteGetProductListItemImp: RemoteGetProductListItemImp
                                                       , private val remoteGetClassificationItemImp: RemoteGetClassificationItemImp):ProductListItemRepository {
   override fun getProductList(pId: Int): Observable<MutableList<Cate>> =
           remoteGetClassificationItemImp.getGetClassificationItem(pId = pId.toString())


    override fun getProductListItem(cateId: Int): Listing<Product> {
        val sourceFactory = ProductListItemDataSourceFactory(remoteGetProductListItemImp,cateId)
        val pagedListConfig = PagedList.Config.Builder()
                .setPageSize(10)
                .setEnablePlaceholders(false)
                .setInitialLoadSizeHint(15)
                .build()
        val pagedList = LivePagedListBuilder(sourceFactory,pagedListConfig).build()
        return Listing(
                pagedList = pagedList
        )
    }
}