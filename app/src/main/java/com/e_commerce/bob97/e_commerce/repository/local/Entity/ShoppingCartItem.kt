package com.e_commerce.bob97.e_commerce.repository.local.Entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "shopping_item")
data class ShoppingCartItem(
        @PrimaryKey
        @SerializedName("proId") var proId: Int,
        @SerializedName("proName") var proName: String,
        @SerializedName("proPrice") var proPrice: Double,
        @SerializedName("proPressCost") var proPressCost: Double,
        @SerializedName("proImg") var proImg: String,
        @SerializedName("proCount") var proCount: Int,
        @SerializedName("onClick")var onClick: Boolean = false
)