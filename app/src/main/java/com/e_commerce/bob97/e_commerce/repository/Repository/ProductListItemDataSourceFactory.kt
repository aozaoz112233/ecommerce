package com.e_commerce.bob97.e_commerce.repository.Repository

import android.arch.paging.DataSource
import com.e_commerce.bob97.e_commerce.repository.data.Product
import com.e_commerce.bob97.e_commerce.repository.remote.RemoteGetProductListItemImp
import javax.inject.Inject

class ProductListItemDataSourceFactory @Inject constructor(private val remoteGetProductListItemImp: RemoteGetProductListItemImp,private val carId:Int):DataSource.Factory<Int,Product>(){
    override fun create(): DataSource<Int, Product> {
        val source = ProductListItemDataSource(remoteGetProductListItemImp,carId)
        return source
    }

}
