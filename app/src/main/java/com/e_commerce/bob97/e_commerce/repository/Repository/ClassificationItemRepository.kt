package com.e_commerce.bob97.e_commerce.repository.Repository

import android.arch.lifecycle.LiveData
import android.arch.paging.PagedList
import com.e_comme.rce.bob97.e_commerce.repository.data.Listing
import com.e_commerce.bob97.e_commerce.repository.data.Cate
import io.reactivex.Observable

interface ClassificationItemRepository{
    fun getClassification(): Observable<MutableList<Cate>>
    fun getClassificationItem(index:String):Listing<Cate>
}