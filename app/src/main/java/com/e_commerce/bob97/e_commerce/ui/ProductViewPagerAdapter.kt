package com.e_commerce.bob97.e_commerce.ui

import android.arch.paging.PagedList
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.e_commerce.bob97.e_commerce.repository.data.Cate

class ProductViewPagerAdapter(fm:FragmentManager?,private val cartList: MutableList<Cate>):FragmentPagerAdapter(fm){

    override fun getItem(position: Int): Fragment {
        val bundle = Bundle()
        bundle.putInt("catePid",cartList[position].cateId)
        val fragment = ProductPageFragment.newInstance()
        fragment.arguments = bundle
        return fragment
    }

    override fun getCount(): Int {
        return cartList.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return cartList[position].cateName
    }

}