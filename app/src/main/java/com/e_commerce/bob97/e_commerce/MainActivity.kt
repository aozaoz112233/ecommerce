package com.e_commerce.bob97.e_commerce

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import kotlinx.android.synthetic.main.activity_main.*



class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
       bottomNavigationView.setupWithNavController(Navigation.findNavController(this,R.id.navigation_host_fragment))
        bottomNavigationView.disableShiftMode()

    }
    private fun replaceFragement(fragment:android.support.v4.app.Fragment){
        supportFragmentManager.beginTransaction()
                .replace(R.id.navigation_host_fragment,fragment)
                .commit()
    }

    override fun onSupportNavigateUp() = Navigation.findNavController(this,R.id.navigation_host_fragment).navigateUp()
}
