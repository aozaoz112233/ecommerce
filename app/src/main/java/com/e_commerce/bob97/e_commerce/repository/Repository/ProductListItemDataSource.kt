package com.e_commerce.bob97.e_commerce.repository.Repository

import android.arch.paging.ItemKeyedDataSource
import com.e_commerce.bob97.e_commerce.repository.data.Product
import com.e_commerce.bob97.e_commerce.repository.remote.RemoteGetProductListItemImp
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ProductListItemDataSource @Inject constructor(private val remoteGetProductListItemImp: RemoteGetProductListItemImp, private val cateId: Int) : ItemKeyedDataSource<Int, Product>() {
    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Product>) {
        val dispose = remoteGetProductListItemImp.getProductListItem(cateId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    callback.onResult(it)
                }

    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Product>) {
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Product>) {

    }

    override fun getKey(item: Product): Int = item.proId

}