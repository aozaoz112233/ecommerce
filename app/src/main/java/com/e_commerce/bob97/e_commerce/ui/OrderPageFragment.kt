package com.e_commerce.bob97.e_commerce.ui

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.e_commerce.bob97.e_commerce.GlideApp
import com.e_commerce.bob97.e_commerce.viewmodel.OrderPageViewModel
import com.e_commerce.bob97.e_commerce.R
import kotlinx.android.synthetic.main.order_page_fragment.*


class OrderPageFragment : Fragment() {

    companion object {
        fun newInstance() = OrderPageFragment()
    }

    private lateinit var viewModel: OrderPageViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.order_page_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(OrderPageViewModel::class.java)
        initUi()

    }
    fun initUi(){
        val glide = GlideApp.with(this)
        val postion = arguments!!.getInt("postion")
        val dispose = viewModel.getUserOrder().subscribe {
            when(postion){
                0 ->
                    orderContentRecyclerView.adapter = OrderItemAdaper(context,glide,it,viewModel,this)
                1->
                    orderContentRecyclerView.adapter = OrderItemAdaper(context,glide,it.filter {
                        it.orderState == "新订单"
                    },viewModel,this)
                2->
                    orderContentRecyclerView.adapter = OrderItemAdaper(context,glide,it.filter {
                        it.orderState == "已付款"
                    },viewModel,this)

            }
        }

        orderContentRecyclerView.layoutManager = LinearLayoutManager(context)
    }

}
