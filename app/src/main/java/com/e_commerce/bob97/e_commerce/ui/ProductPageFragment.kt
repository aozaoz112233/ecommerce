package com.e_commerce.bob97.e_commerce.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment
import com.e_commerce.bob97.e_commerce.GlideApp
import com.e_commerce.bob97.e_commerce.viewmodel.ProductPageViewModel
import com.e_commerce.bob97.e_commerce.R
import kotlinx.android.synthetic.main.product_page_fragment.*


class ProductPageFragment : Fragment() {

    companion object {
        fun newInstance() = ProductPageFragment()
    }

    private lateinit var viewModel: ProductPageViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.product_page_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ProductPageViewModel::class.java)
        initUi()
    }
    fun initUi(){
        val glide = GlideApp.with(this)
        val adapter = ProductListItemAdapter(glide = glide,didSelect ={
            it ->
            NavHostFragment.findNavController(this.parentFragment!!).navigate(R.id.action_productListFragment_to_commodityFragment,it)
        })
        val layoutManager = LinearLayoutManager(context)
        val cateId = arguments!!.getInt("catePid")
        ProductPageRecyclerView.adapter = adapter
        ProductPageRecyclerView.layoutManager = layoutManager
        viewModel.getItem(cateId).observe(this, Observer {
            adapter.submitList(it)
        })

    }

}
