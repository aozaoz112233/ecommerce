package com.e_commerce.bob97.e_commerce.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel;
import com.e_commerce.bob97.e_commerce.di.EconApplication
import com.e_commerce.bob97.e_commerce.repository.Repository.ClassificationItemRepositoryImp
import com.e_commerce.bob97.e_commerce.repository.Repository.ProductListItemRepositoryImp
import javax.inject.Inject

class ProductPageViewModel : ViewModel() {
    @Inject
    lateinit var  repository: ProductListItemRepositoryImp
    init {
        initializeDagger()
    }

    private  val subItemIdx = MutableLiveData<String>()
    fun getItem(cartId:Int) =
            repository.getProductListItem(cartId)?.pagedList

    override fun onCleared() {
        super.onCleared()
    }
    private fun initializeDagger() = EconApplication.appComponent.inject(this)
}
