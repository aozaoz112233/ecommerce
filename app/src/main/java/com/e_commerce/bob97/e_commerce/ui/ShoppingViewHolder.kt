package com.e_commerce.bob97.e_commerce.ui

import android.support.v7.widget.AppCompatCheckBox
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.e_commerce.bob97.e_commerce.GlideRequests
import com.e_commerce.bob97.e_commerce.R
import com.e_commerce.bob97.e_commerce.repository.local.Entity.ShoppingCartItem
import com.e_commerce.bob97.e_commerce.repository.remote.RemoteContract

class ShoppingViewHolder(view:View,private val glide: GlideRequests):RecyclerView.ViewHolder(view) {
    private val image: ImageView = view.findViewById(R.id.shoppingCartItemImage)
    private val title:TextView = view.findViewById(R.id.shoppingCartItemTitle)
    private val price:TextView = view.findViewById(R.id.shoppingCartItemPrice)
    private val num:EditText = view.findViewById(R.id.item_num)
    private val checkBox:AppCompatCheckBox = view.findViewById(R.id.cartItemCheckbox)


    fun bind(post:ShoppingCartItem){
        title.text = post.proName
        price.text = post.proPrice.toString()
        glide.load(RemoteContract.IMAGEHOST + post.proImg)
                .centerCrop()
                .into(image)
        num.setText(post.proCount.toString())
        checkBox.isChecked = post.onClick
    }

    companion object {
        fun creat(parent:ViewGroup,glide: GlideRequests): ShoppingViewHolder {
            val  view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_shoppingcart,parent,false)
            return ShoppingViewHolder(view, glide)
        }
    }
}