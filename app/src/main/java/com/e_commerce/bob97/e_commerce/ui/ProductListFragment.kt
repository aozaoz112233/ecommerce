package com.e_commerce.bob97.e_commerce.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.e_commerce.bob97.e_commerce.R
import com.e_commerce.bob97.e_commerce.viewmodel.ProductListViewModel
import kotlinx.android.synthetic.main.product_list_fragment.*


class ProductListFragment : Fragment() {

    companion object {
        fun newInstance() = ProductListFragment()
    }

    private lateinit var viewModel: ProductListViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.product_list_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ProductListViewModel::class.java)
        initUi()
    }


    fun initUi() {
        val catePid = arguments!!.getInt("catePid")
        val postion = arguments!!.getInt("postion")
        val dispose = viewModel.getClassificationInfo(catePid).subscribe {
            val adapter = ProductViewPagerAdapter(childFragmentManager,it)
            productListViewPager.adapter = adapter
            productListTab.setupWithViewPager(productListViewPager)
            productListTab.getTabAt(postion)?.select()
        }


        }

}
