package com.e_commerce.bob97.e_commerce.repository.data

import com.google.gson.annotations.SerializedName

data class OrderItem(
        @SerializedName("orderItemId")val orderItemId: Int,
        @SerializedName("orderId")val orderId: String,
        @SerializedName("proId")val proId: Int,
        @SerializedName("product")val product: Product,
        @SerializedName("proCount")val proCount: Int,
        @SerializedName("orderItemSumPrice")val orderItemSumPrice: Int
)