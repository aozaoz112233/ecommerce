package com.e_commerce.bob97.e_commerce.repository.remote

import io.reactivex.Observable
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface RemotePostRegisterService {
    @FormUrlEncoded
    @POST(RemoteContract.REGISTER)
    fun postRegister(@Field("userTelphone")userTelphone:String, @Field("userPassword")userPassword:String):Observable<HashMap<String,String>>
}