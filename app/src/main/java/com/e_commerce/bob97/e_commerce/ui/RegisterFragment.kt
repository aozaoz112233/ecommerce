package com.e_commerce.bob97.e_commerce.ui

import android.app.AlertDialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment
import com.e_commerce.bob97.e_commerce.R
import com.e_commerce.bob97.e_commerce.viewmodel.RegisterViewModel
import kotlinx.android.synthetic.main.login_fragment.*
import kotlinx.android.synthetic.main.register_fragment.*


class RegisterFragment : Fragment() {

    companion object {
        fun newInstance() = RegisterFragment()
    }

    private lateinit var viewModel: RegisterViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.register_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(RegisterViewModel::class.java)
        initUi()
    }

    fun initUi() {
        registerPhone.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if (s?.length == 11){
                    registerCodeButton.setBackgroundResource(R.color.colorRed)
                registerCodeButton.isClickable = true
                viewModel.editTextCheck["phone"] = true}
                else {
                    registerCodeButton.setBackgroundResource(R.color.colorGray)
                    registerCodeButton.isClickable = false
                    viewModel.editTextCheck["phone"] = false
                }
            }
        })
        registerCode.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s?.length == 6) {
                    viewModel.editTextCheck["code"] = true
                }
                else viewModel.editTextCheck["code"] = false
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })
        registerPassword.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if (s?.length != 0)
                    viewModel.editTextCheck["password"] = true
                else
                    viewModel.editTextCheck["password"] = false
            }

        })
        registerButton.setOnClickListener {
            viewModel.register(registerPhone.text.toString(), registerPassword.text.toString())
                    .subscribe {
                        if (it.containsValue("success")) {
                            viewModel.CodeTimeCount.cancel()
                            viewModel.RemoteloadUserinfo(registerPhone.text.toString(), registerPassword.text.toString())!!
                                    .observe(this, Observer {
                                        viewModel.saveUserinfo(it)
                                    })
                            val alert = AlertDialog.Builder(this.context)
                                    .setMessage("注册成功")
                                    .show()
                            NavHostFragment.findNavController(this).navigate(R.id.action_registerFragment_to_personalFragment)
                        } else {
                            val alert = AlertDialog.Builder(this.context)
                                    .setMessage("注册失败")
                                    .show()
                        }
                    }
        }
        viewModel.setAllEditTextLisner { isClickable, color ->
            registerButton.isClickable = isClickable
            registerButton.setBackgroundResource(color)
        }
        registerCodeButton.setOnClickListener {
            viewModel.setCodeButtonLisener { isClickable, buttonString ->
                registerCodeButton.isClickable = isClickable
                registerCodeButton.text = buttonString
            }
        }

    }

}
