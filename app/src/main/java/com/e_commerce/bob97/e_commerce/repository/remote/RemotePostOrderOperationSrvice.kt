package com.e_commerce.bob97.e_commerce.repository.remote

import com.e_commerce.bob97.e_commerce.repository.data.Order
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface RemotePostOrderOperationSrvice {
    @POST(RemoteContract.ORDEROPERATION)
    fun remotePostOrderOperation(@Query("orderId")orderId:String,@Query("orderState")orderState:String):Observable<HashMap<String,String>>
}