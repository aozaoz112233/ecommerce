package com.e_commerce.bob97.e_commerce.repository.remote

import javax.inject.Inject

class RemotePostRegisterImp @Inject constructor(private val remotePostRegisterService: RemotePostRegisterService) {
    fun postPostRegister(userTelphone:String,userPassword:String) =
            remotePostRegisterService.postRegister(userTelphone,userPassword)
}