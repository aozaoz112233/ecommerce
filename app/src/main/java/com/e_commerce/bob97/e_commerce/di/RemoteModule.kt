package com.e_commerce.bob97.e_commerce.di


import com.e_commerce.bob97.e_commerce.repository.remote.*
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class RemoteModule {
    @Provides @Singleton fun provideGson():Gson =
            GsonBuilder()
                    .create()


    @Provides @Singleton fun provideOkHttpClient() :OkHttpClient =
        OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build()

    @Provides @Singleton fun provideRetrofit( gson: Gson,okHttpClient: OkHttpClient) :Retrofit =
        Retrofit.Builder()
                .baseUrl(RemoteContract.HOST)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build()

    @Provides @Singleton fun privideRomoteGetUserService(retrofit: Retrofit):RemoteGetUserService =
            retrofit.create(RemoteGetUserService::class.java)
    @Provides @Singleton fun privideRemoteGetClassificationItemService(retrofit: Retrofit):RemoteGetClassificationItemService=
            retrofit.create(RemoteGetClassificationItemService::class.java)
    @Provides @Singleton fun privideRemoteGetProductListItemService(retrofit: Retrofit):RemoteGetProductListItemService=
            retrofit.create(RemoteGetProductListItemService::class.java)
    @Provides @Singleton fun privideRemoteGetCommodityImp(retrofit: Retrofit):RemoteGetCommodityService =
            retrofit.create(RemoteGetCommodityService::class.java)
    @Provides @Singleton fun privideRemoteGetClassificationService(retrofit: Retrofit):RemoteGetClassificationService=
            retrofit.create(RemoteGetClassificationService::class.java)
    @Provides @Singleton fun privideRemotePostCateOrderService(retrofit: Retrofit):RemotePostCateOrderService=
            retrofit.create(RemotePostCateOrderService::class.java)
    @Provides @Singleton fun privideRemoteGetOrderSrvice(retrofit: Retrofit):RemoteGetOrderSrvice =
            retrofit.create(RemoteGetOrderSrvice::class.java)
    @Provides @Singleton fun privideRemotePostRegisterService(retrofit: Retrofit):RemotePostRegisterService =
            retrofit.create(RemotePostRegisterService::class.java)
    @Provides @Singleton fun privideRemoteGetCodeService(retrofit: Retrofit):RemoteGetCodeService =
            retrofit.create(RemoteGetCodeService::class.java)
    @Provides @Singleton fun privideRemotePostOrderOperationSrvice(retrofit: Retrofit):RemotePostOrderOperationSrvice =
            retrofit.create(RemotePostOrderOperationSrvice::class.java)
}