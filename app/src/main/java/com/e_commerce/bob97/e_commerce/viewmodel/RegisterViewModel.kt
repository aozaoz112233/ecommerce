package com.e_commerce.bob97.e_commerce.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel;
import android.os.CountDownTimer
import com.e_commerce.bob97.e_commerce.R
import com.e_commerce.bob97.e_commerce.di.EconApplication
import com.e_commerce.bob97.e_commerce.repository.Repository.UserRepositoryImp
import com.e_commerce.bob97.e_commerce.repository.data.Account
import javax.inject.Inject

class RegisterViewModel : ViewModel() {
    @Inject
    lateinit var userRepositoryImp: UserRepositoryImp
    @Inject
    lateinit var contex: EconApplication

    lateinit var CodeTimeCount:CountDownTimer

    private fun initializeDagger() = EconApplication.appComponent.inject(this)

    var editTextCheck = mutableMapOf<String, Boolean>(Pair("phone", false), Pair("code", false), Pair("password", false))

    private var userinfo: LiveData<Account>? = null

    init {
        initializeDagger()
    }

    fun register(userTelphone: String, userPassword: String) = userRepositoryImp.postResiter(userTelphone, userPassword)

    fun RemoteloadUserinfo(phone: String, password: String): LiveData<Account>? {
        if (userinfo == null)
            userinfo = MutableLiveData()
        userinfo = userRepositoryImp.getUserInfo(phone, password)
        return userinfo
    }

    fun saveUserinfo(account: Account?) {
        contex.insertAccount(account!!)
    }

    fun setAllEditTextLisner(listener:(isClickable:Boolean,color:Int)->Unit) {
        if (!editTextCheck.containsValue(false))
            listener(true,R.color.colorButton)
    }

    fun setCodeButtonLisener(listener: (isClickable:Boolean,buttonString:String) -> Unit){
        CodeTimeCount = object :CountDownTimer(60000,1000){
            override fun onFinish() {
                listener(true,"获取验证码")
            }
            override fun onTick(millisUntilFinished: Long) {
                listener(false,(millisUntilFinished/1000).toString())
            }

        }.start()
    }



}
