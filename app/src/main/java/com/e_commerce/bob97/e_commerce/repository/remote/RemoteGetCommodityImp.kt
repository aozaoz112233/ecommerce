package com.e_commerce.bob97.e_commerce.repository.remote

import javax.inject.Inject

class RemoteGetCommodityImp @Inject constructor(private val remoteGetCommodityService: RemoteGetCommodityService){
    fun getCommodity(proId:Int) =
            remoteGetCommodityService.getCommodity(proId)

}