package com.e_commerce.bob97.e_commerce.repository.local.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.e_commerce.bob97.e_commerce.repository.data.Account

@Dao
interface AccountDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(account : Account?)

    @Query("SELECT * FROM account")
    fun getAccountInfo():Account

}