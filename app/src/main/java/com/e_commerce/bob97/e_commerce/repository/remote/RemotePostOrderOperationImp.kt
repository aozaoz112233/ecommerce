package com.e_commerce.bob97.e_commerce.repository.remote

import com.e_commerce.bob97.e_commerce.repository.data.CateOrder
import javax.inject.Inject

class RemotePostOrderOperationImp @Inject constructor(private val remotePostOrderOperationSrvice: RemotePostOrderOperationSrvice){
    fun postOrderOperation(orderId:String,orderState:String) =
            remotePostOrderOperationSrvice.remotePostOrderOperation(orderId,orderState)
}