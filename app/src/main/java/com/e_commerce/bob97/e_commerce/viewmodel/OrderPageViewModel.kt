package com.e_commerce.bob97.e_commerce.viewmodel

import android.arch.lifecycle.ViewModel;
import com.e_commerce.bob97.e_commerce.di.EconApplication
import com.e_commerce.bob97.e_commerce.repository.Repository.OrderRepositoryImp
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class OrderPageViewModel : ViewModel() {
    @Inject
    lateinit var orderRepositoryImp: OrderRepositoryImp
    @Inject
    lateinit var contex:EconApplication


    init {
        initializeDagger()
    }
    fun getUserOrder() = orderRepositoryImp.getOrder(contex.getAccount().userId.toString())

    private fun initializeDagger() = EconApplication.appComponent.inject(this)

    fun postOrderOperation(orderId: String, orderState: String) = orderRepositoryImp.postOrderOperation(orderId,orderState)

}
