package com.e_commerce.bob97.e_commerce.repository.Repository

import android.arch.paging.ItemKeyedDataSource
import com.e_commerce.bob97.e_commerce.repository.data.Cate
import com.e_commerce.bob97.e_commerce.repository.remote.RemoteGetClassificationItemImp
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ClassificationItemDataSource@Inject constructor(private val remoteGetClassificationItemImp: RemoteGetClassificationItemImp, private val index:String):ItemKeyedDataSource<String,Cate>(){
    override fun loadInitial(params: LoadInitialParams<String>, callback: LoadInitialCallback<Cate>) {
        val disposable = remoteGetClassificationItemImp.getGetClassificationItem(pId = index)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe {
                    callback.onResult(it)
                }
    }

    override fun loadAfter(params: LoadParams<String>, callback: LoadCallback<Cate>) {
    }

    override fun loadBefore(params: LoadParams<String>, callback: LoadCallback<Cate>) {
    }

    override fun getKey(item: Cate): String = item.cateId.toString()

}