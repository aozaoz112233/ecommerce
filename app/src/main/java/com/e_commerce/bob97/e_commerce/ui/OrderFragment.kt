package com.e_commerce.bob97.e_commerce.ui

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.e_commerce.bob97.e_commerce.GlideApp
import com.e_commerce.bob97.e_commerce.R
import com.e_commerce.bob97.e_commerce.viewmodel.OrderViewModel
import kotlinx.android.synthetic.main.order_fragment.*


class OrderFragment : Fragment() {

    companion object {
        fun newInstance() = OrderFragment()
    }

    private lateinit var viewModel: OrderViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.order_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(OrderViewModel::class.java)
        initUi()
    }
    fun initUi(){
        val adapter = OrderViewPagerAdapter(childFragmentManager)
        orderViewPager.adapter = adapter
        orderTab.setupWithViewPager(orderViewPager)
    }

}
