package com.e_commerce.bob97.e_commerce.repository.remote

import javax.inject.Inject

class RemoteGetCodeImp @Inject constructor(private val remoteGetCodeService: RemoteGetCodeService) {
    fun getCode() =
            remoteGetCodeService.getCode()
}