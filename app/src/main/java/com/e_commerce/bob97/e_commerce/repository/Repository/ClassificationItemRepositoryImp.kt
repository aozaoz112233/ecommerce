package com.e_commerce.bob97.e_commerce.repository.Repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.e_comme.rce.bob97.e_commerce.repository.data.Listing
import com.e_commerce.bob97.e_commerce.repository.data.Cate
import com.e_commerce.bob97.e_commerce.repository.remote.RemoteGetClassificationImp
import com.e_commerce.bob97.e_commerce.repository.remote.RemoteGetClassificationItemImp
import io.reactivex.Observable
import javax.inject.Inject


class ClassificationItemRepositoryImp @Inject constructor(private val remoteGetClassificationItemImp: RemoteGetClassificationItemImp
,private val remoteGetClassification:RemoteGetClassificationImp):ClassificationItemRepository {
    override fun getClassification(): Observable<MutableList<Cate>> {
        return remoteGetClassification.getGetClassification()
    }

    override fun getClassificationItem(index: String): Listing<Cate> {
        val sourceFactory = ClassificationItemDataSourceFactory(remoteGetClassificationItemImp,index)
        val pagedListConfig = PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPageSize(10)
                .setInitialLoadSizeHint(20)
                .build()
        val pagedList = LivePagedListBuilder(sourceFactory,pagedListConfig)
                .build()
        return Listing(
                pagedList = pagedList
        )
    }


}