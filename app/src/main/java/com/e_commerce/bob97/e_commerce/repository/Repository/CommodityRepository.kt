package com.e_commerce.bob97.e_commerce.repository.Repository

import android.arch.lifecycle.LiveData
import com.e_commerce.bob97.e_commerce.repository.data.Product

interface CommodityRepository {
    fun getCommodityInfo(proId:Int):LiveData<Product>
}