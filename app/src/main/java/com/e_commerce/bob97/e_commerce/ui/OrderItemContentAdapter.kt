package com.e_commerce.bob97.e_commerce.ui

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.e_commerce.bob97.e_commerce.GlideRequests
import com.e_commerce.bob97.e_commerce.repository.data.OrderItem
import com.e_commerce.bob97.e_commerce.repository.remote.RemoteContract
import com.e_commerce.bob97.e_commerce.viewmodel.OrderPageViewModel
import kotlinx.android.synthetic.main.order_item.view.*
import kotlinx.android.synthetic.main.order_item_content.view.*

class OrderItemContentAdapter(private val glide: GlideRequests,private val orderItems:List<OrderItem>):RecyclerView.Adapter<OrderItemContentViewHolder>() {
    override fun getItemCount(): Int {
        return orderItems.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderItemContentViewHolder {
        return OrderItemContentViewHolder.creat(parent)

    }

    override fun onBindViewHolder(holder: OrderItemContentViewHolder, position: Int) {
        glide.load(RemoteContract.IMAGEHOST+orderItems[position].product.proImg)
                .into(holder.itemView.OrderListItemImage)
        holder.itemView.OrderListItemTitle.text = orderItems[position].product.proName
        holder.itemView.OrderListItemPrice.text = orderItems[position].product.proPrice.toString()
        holder.itemView.orderItemBuyNum.text = "X"+ orderItems[position].proCount

    }
}