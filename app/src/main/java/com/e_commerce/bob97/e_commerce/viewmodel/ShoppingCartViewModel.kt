package com.e_commerce.bob97.e_commerce.viewmodel

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel;
import com.e_commerce.bob97.e_commerce.di.EconApplication
import com.e_commerce.bob97.e_commerce.repository.Repository.OrderRepositoryImp
import com.e_commerce.bob97.e_commerce.repository.data.CateOrder
import com.e_commerce.bob97.e_commerce.repository.local.Entity.ShoppingCartItem
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ShoppingCartViewModel : ViewModel() {
    @Inject
    lateinit var contex: EconApplication
    @Inject
    lateinit var orderRepositoryImp: OrderRepositoryImp


    init {
        initializeDagger()
    }

    fun selectAll(flag: Boolean) {
        if (flag)
            contex.getShoppingCatItems().map { it.onClick = true }
        else contex.getShoppingCatItems().map { it.onClick = false }
    }

            fun allPrice() = Observable.create(ObservableOnSubscribe<String> {
            var allprice:Double = 0.0
            var pressCost:Double = 0.0
            contex.getShoppingCatItems().filter {
                it.onClick == true
            }.map {
                allprice += it.proPrice * it.proCount + it.proPressCost
                pressCost += it.proPressCost
            }
        it.onNext("合计："+ allprice + ("(包含运费") + pressCost + ")")
    }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

    fun postCateOrder() = orderRepositoryImp.postCateOrder(CateOrder(contex.getAccount().userId,contex.getShoppingCatItems()))


    fun deleteCart(){
        contex.getShoppingCatItems().filter {
            it.onClick == true
        }.map {
            contex.getShoppingCatItems().remove(it)
        }
    }





    private fun initializeDagger() = EconApplication.appComponent.inject(this)
}
