package com.e_commerce.bob97.e_commerce.viewmodel

import android.arch.lifecycle.ViewModel;
import com.e_commerce.bob97.e_commerce.di.EconApplication
import com.e_commerce.bob97.e_commerce.repository.Repository.ClassificationItemRepositoryImp
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ClassificationViewModel : ViewModel() {
    @Inject
    lateinit var classificationItemRepositoryImp:ClassificationItemRepositoryImp
    init {
        initializeDagger()
    }
    fun getClassificationInfo() = classificationItemRepositoryImp.getClassification()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())




    private fun initializeDagger() = EconApplication.appComponent.inject(this)

}
