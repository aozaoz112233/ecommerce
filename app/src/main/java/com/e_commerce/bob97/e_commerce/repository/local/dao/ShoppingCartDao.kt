package com.e_commerce.bob97.e_commerce.repository.local.dao

import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.e_commerce.bob97.e_commerce.repository.local.Entity.ShoppingCart

interface ShoppingCartDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(shoppingCart: ShoppingCart)

    @Query("SELECT * FROM shopping_cart")
    fun getShoppingCart():ShoppingCart
}