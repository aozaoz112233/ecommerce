package com.e_commerce.bob97.e_commerce.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.e_commerce.bob97.e_commerce.GlideRequests
import com.e_commerce.bob97.e_commerce.R
import com.e_commerce.bob97.e_commerce.repository.data.Cate
import com.e_commerce.bob97.e_commerce.repository.remote.RemoteContract
import java.util.*

class ClassificationItemViewHolder(view:View,private val glide: GlideRequests,internal val didSelect:(idx:Bundle)->Unit):RecyclerView.ViewHolder(view){
    private val image:ImageView = view.findViewById(R.id.classificationItemImageView)
    private val title:TextView = view.findViewById(R.id.classificationItemTextView)
    private var positon:Int? = null
    private var catePid:Int? = null

    init {
        view.setOnClickListener{
            val bundle = Bundle()
            bundle.putInt("postion",positon!!)
            bundle.putInt("catePid",catePid!!)
            didSelect(bundle)
        }
    }



    fun bind(post: Cate?,positon:Int){
        title.text = post?.cateName
        glide.load(RemoteContract.IMAGEHOST + post?.cateIcon)
                .centerCrop()
                .into(image)
        val random = Random()
        this.positon = positon
        this.catePid = post?.catePid
    }

    companion object {
        fun creat(parent:ViewGroup,glide: GlideRequests,didSelect:(idx:Bundle)->Unit):ClassificationItemViewHolder{
            val  view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_classification,parent,false)
            return ClassificationItemViewHolder(view,glide,didSelect)

        }
    }

}