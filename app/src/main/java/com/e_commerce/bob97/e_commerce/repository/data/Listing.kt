package com.e_comme.rce.bob97.e_commerce.repository.data

import android.arch.lifecycle.LiveData
import android.arch.paging.PagedList

data class Listing<T>(
        val pagedList: LiveData<PagedList<T>>
)