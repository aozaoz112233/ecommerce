package com.e_commerce.bob97.e_commerce.ui

import android.app.Activity
import android.app.AlertDialog
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.navigation.fragment.NavHostFragment
import com.bumptech.glide.Glide
import com.e_commerce.bob97.e_commerce.GlideApp
import com.e_commerce.bob97.e_commerce.R
import com.e_commerce.bob97.e_commerce.repository.data.Account
import com.e_commerce.bob97.e_commerce.repository.remote.RemoteContract
import com.e_commerce.bob97.e_commerce.viewmodel.PersonalViewModel
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.Observer
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.personal_fragment.*
import kotlinx.android.synthetic.main.personal_head.*
import java.util.*

class PersonalFragment : Fragment(),NavigationView.OnNavigationItemSelectedListener{

    companion object {
        fun newInstance() = PersonalFragment()
    }

    private lateinit var viewModel: PersonalViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.personal_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PersonalViewModel::class.java)
        initUi()
    }
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.personal_menu_order ->
                if (viewModel.islogined()){
                NavHostFragment.findNavController(this).navigate(R.id.action_personalFragment_to_orderFragment)
            }
            else  {val alert = AlertDialog.Builder(this.context)
                .setMessage("请先登录")
                .show()}
        }
        return true
    }

    private fun initUi(){
        (activity as Activity).bottomNavigationView.visibility = View.VISIBLE
        personalNav.setNavigationItemSelectedListener(this)
        personalNav.getHeaderView(0).findViewById<ImageView>(R.id.personalIcon).setOnClickListener {
            if (!viewModel.islogined()){
                NavHostFragment.findNavController(this).navigate(R.id.action_personalFragment_to_loginFragment)
                (activity as Activity).bottomNavigationView.visibility = View.GONE
            }
        }

        if (viewModel.islogined()) {
            val dispose = Observable.create(ObservableOnSubscribe<Account> {
                it.onNext(viewModel.logined())
            })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        textView_username.text = it.userName
                        GlideApp.with(this)
                                .load(RemoteContract.IMAGEHOST + it.userImg)
                                .into(personalIcon)
                    }
        }


    }



}