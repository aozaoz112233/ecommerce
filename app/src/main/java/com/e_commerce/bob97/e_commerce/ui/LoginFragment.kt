package com.e_commerce.bob97.e_commerce.ui

import android.app.Activity
import android.app.AlertDialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import com.bumptech.glide.load.engine.Resource
import com.e_commerce.bob97.e_commerce.viewmodel.LoginViewModel
import com.e_commerce.bob97.e_commerce.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.login_fragment.*
import kotlinx.android.synthetic.main.register_fragment.*


class LoginFragment : Fragment(),Toolbar.OnMenuItemClickListener{
    companion object {
        fun newInstance() = LoginFragment()
    }

    private lateinit var viewModel: LoginViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.login_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        initUi()
    }
    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when(item?.itemId){
            R.id.home ->
                NavHostFragment.findNavController(this).navigate(R.id.action_loginFragment_to_personalFragment)
            R.id.register ->
                NavHostFragment.findNavController(this).navigate(R.id.action_loginFragment_to_registerFragment)
        }
        return false
    }
    fun initUi(){
        register.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.action_loginFragment_to_registerFragment)
        }
        loginPhone.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                if (s?.length == 11){
                    viewModel.editTextCheck["phone"] = true
                }
                else{
                    viewModel.editTextCheck["phone"] = false
                }
                viewModel.setEditTextLisnerlistener { isClickable, color ->
                    loginButton.isClickable = isClickable
                    loginButton.setBackgroundResource(color)
                }
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })
        loginPassword.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                viewModel.setEditTextLisnerlistener { isClickable, color ->
                    loginButton.isClickable = isClickable
                    loginButton.setBackgroundResource(color)
                }
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s?.length != 0){
                    viewModel.editTextCheck["password"] = true
                }
                else{
                    viewModel.editTextCheck["password"] = false
                }
            }

        })
        loginToolbar.setOnMenuItemClickListener(this)
        loginButton.setOnClickListener {
            viewModel.RemoteloadUserinfo(loginPhone.text.toString(),loginPassword.text.toString())?.observe(
                    this, Observer {
                if (it?.userName == null){
                    val alert = AlertDialog.Builder(this.context)
                            .setMessage("登录失败,账号或密码错误")
                            .show()
                }
                else{
                viewModel.saveUserinfo(it)
                NavHostFragment.findNavController(this).navigate(R.id.action_loginFragment_to_personalFragment)
                }
                    })

        }
    }

}
