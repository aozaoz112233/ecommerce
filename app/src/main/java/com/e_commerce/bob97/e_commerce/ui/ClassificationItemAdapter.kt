package com.e_commerce.bob97.e_commerce.ui

import android.arch.paging.PagedListAdapter
import android.os.Bundle
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.e_commerce.bob97.e_commerce.GlideRequests
import com.e_commerce.bob97.e_commerce.repository.data.Cate

class ClassificationItemAdapter(private val glide: GlideRequests,internal val didSelect:(idx: Bundle)->Unit):PagedListAdapter<Cate,RecyclerView.ViewHolder>(diffUtil) {
    companion object {
        private val diffUtil = object :DiffUtil.ItemCallback<Cate>(){
            override fun areContentsTheSame(oldItem: Cate?, newItem: Cate?): Boolean =
                oldItem == newItem

            override fun areItemsTheSame(oldItem: Cate?, newItem: Cate?): Boolean =
                    oldItem?.cateId == newItem?.cateId
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ClassificationItemViewHolder.creat(parent,glide,didSelect)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            (holder as ClassificationItemViewHolder).bind(getItem(position),position)
    }

    override fun getItemCount(): Int {
        return super.getItemCount()
    }
}

