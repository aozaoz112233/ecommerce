package com.e_commerce.bob97.e_commerce.repository.remote

import com.e_commerce.bob97.e_commerce.repository.data.CateOrder
import javax.inject.Inject

class RemotePostCateOrderImp @Inject constructor(private val remotePostCateOrderService: RemotePostCateOrderService){
    fun postCateOrder(cateOrder: CateOrder) =
            remotePostCateOrderService.postCateOrder(cateOrder)
}