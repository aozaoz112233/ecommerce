package com.e_commerce.bob97.e_commerce.repository.data

import com.google.gson.annotations.SerializedName

data class Product(

        //商品ID
        @SerializedName("proId") var proId: Int,

        //商品名称
        @SerializedName("proName") var proName: String,

        //图片
        @SerializedName("proImg") var proImg: String,

        //商品进价
        @SerializedName("proPurchase") var proPurchase: Double,

        //商品售价
        @SerializedName("proPrice") var proPrice: Double,

        //类别ID
        @SerializedName("cateId") var cateId: Int,

        //商品品牌
        @SerializedName("proBrand") var proBrand: String,

        //经销商
        @SerializedName("proDealer") var proDealer: String,

        //商品描述
        @SerializedName("proDesp") var proDesp: String,

        //快递方信息
        @SerializedName("proPress") var proPress: String,

        //快递费用
        @SerializedName("proPressCost") var proPressCost: Double,

        //月销量

        @SerializedName("proMonthlySales") var proMonthlySales: Int,

        //发货地
        @SerializedName("proPlace") var proPlace: String


)