package com.e_commerce.bob97.e_commerce.di

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val application: EconApplication) {
    @Provides @Singleton fun provideContext():Context = application
    @Provides @Singleton fun provideApplication():EconApplication = application
}