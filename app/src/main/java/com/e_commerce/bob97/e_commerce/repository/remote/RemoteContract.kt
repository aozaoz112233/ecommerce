package com.e_commerce.bob97.e_commerce.repository.remote


class RemoteContract {
    companion object {
        const val HOST = "http://10.25.130.138:8080/commerce_V2/"
        //const val HOST = "http://192.168.1.3:8080/commerce_V2/"
        const val IMAGEHOST = "http://10.25.130.138:8080"
        const val LOGIN = "userLogin"
        const val CLASSIFICATION = "user/firstCate"
        const val CLASSIFICATIONITEM = "user/secondCate"
        const val PRODUCTLIST = "user/getProByCate"
        const val PRODUCT = "user/getPro"
        const val POSTCATEORDER = "user/addOrder"
        const val ORDER = "user/queryOrders"
        const val REGISTER = "user/register"
        const val CODE = "user/getCode"
        const val ORDEROPERATION = "user/updateOrder"

    }

}